'use strict';

const https = require('https');

const TELEGRAM_TOKEN = process.env.TELEGRAM_TOKEN;
const URL = `https://api.telegram.org/bot${TELEGRAM_TOKEN}/`

module.exports.powerOutageBot = async event => {
  console.log('---- powerOutageBot request method: ' + event.httpMethod);
  if(!TELEGRAM_TOKEN) {
    console.error('TELEGRAM_TOKEN is not provided');
    return {
      statusCode: 500
    };
  }
  if(event.httpMethod === 'POST' && TELEGRAM_TOKEN) {
    let request = JSON.parse(event.body);
    console.log('request: ' + event.body);
    if(request.message) {
      let chatId = request.message.chat.id
      let text = request.message.text
      console.log(`chat_id: ${chatId}`);
      console.log(`text: ${text}`);
      
      let reply = 'Not implemented yet';
      if(text) {
        reply = `Command ${text} is not implemented yet`
      }
      if(chatId) {
        let sendRes = await sendMessage(reply, chatId)
        console.log('sendRes: ' + sendRes);
      }
    }
  }

  return {
    statusCode: 200
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

function sendMessage(text, chatId) {
  return new Promise((resolve, reject) => {
      let encodedText = encodeURI(text);
      let responseUrl = `${URL}sendMessage?text=${encodedText}&chat_id=${chatId}`
      console.log('---- sending request: ' + text + ' to ' + chatId)
      https.get(responseUrl, resp => {
          resp.on('data', data => {
              console.log('data received: ' + data);
          });
          resp.on('end', () => {
              console.log('message is sent');
              resolve();
          });
          resp.on('error', err => {
              console.log('error: ' + err);
              reject(err);
          })
      });
  });
}
