Power outage bot collect and aggregate information about electrical power outages in an area. Any user with a [Telegram](http://telegram.org) account can report an outage in his position. 

Multiple reports aggregated and can be shown in a map on city level. The size of a circle means aggregated number of reports, the color means average duration of the outage.
For example:

![Power Outage Map example](./docs/images/outage-map-example.png)

Bot commands
============

- `/start` - start bot. Prints greeting message and usage information. Request default user location.
- `/outage` - reports outage. Confirms location.
- `/power-back` - power resored.
- `/show-map` - display outage map.

Functionality
=============

After `/outage` command sent, bot ask locations and saves informations about outage. One user can report only one outage, to avoid spamming (however in dev/QA environment might be useful to allow report multiple locations per user). 
Bot asks user to report back after power is restored to keep the map up to date.

In one hour (this should be global configurable parameter for the bot) bot ask back if power is restored. If user does not confirm outage status, bot repeats question every hour and in 3 hours (also configurable parameter) power considered restored. If user confirm outage, the auto-resolution time is set to 3 hours again.

It make sense to avoid sending outage confirmation request at night time.

Configuration
=============

Commands
--------

```
outage - reports outage. Confirms location.
power-back - power resored.
show-map - display outage map.
help - Help message
```


Environment variables
---------------------

- `TELEGRAM_TOKEN` - see https://core.telegram.org/bots/api#authorizing-your-bot

Setting Web Hook
----------------

```
curl "https://api.telegram.org/bot<bot-token>/setWebHook?url=<ApiGatewayUrl>"
```
